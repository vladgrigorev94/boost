<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [App\Http\Controllers\MainController::class, 'show'])
    ->name('main');
Route::get('/contacts', [App\Http\Controllers\ContactController::class, 'show'])
    ->name('contacts');
Route::post('/contact', [App\Http\Controllers\ContactController::class, 'send'])
    ->name('contact.send');
Route::get('/game/{name}', [App\Http\Controllers\GamesController::class, 'show'])
    ->name('game');
Route::get('/product/{id}', [App\Http\Controllers\GamesController::class, 'product'])
    ->name('product');
Route::post('/order', [App\Http\Controllers\GamesController::class, 'orderHandler'])
    ->name('order');
Route::get('/blog', [App\Http\Controllers\MainController::class, 'blogs'])
    ->name('blog');
Route::get('/blog/{slug}', [App\Http\Controllers\MainController::class, 'blog'])
    ->name('blog.item');
Route::get('/reviews', [App\Http\Controllers\MainController::class, 'reviews'])
    ->name('reviews');

require_once __DIR__ . '/web/auth.php';
require_once __DIR__ . '/web/profile.php';

Route::get('{slug}', [App\Http\Controllers\MainController::class, 'page'])
    ->where(['slug' => '((?!admin).)*'])
    ->name('page');
