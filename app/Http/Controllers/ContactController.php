<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\ContactRequest;
use App\Models\Contact;
use Arr;

class ContactController extends Controller
{
    //

    public function show() {

        return view('contacts');
    }

    public function send(ContactRequest $request) {
        $data = $request->all();
        $contact = new Contact;
        $contact->name = Arr::get($data, 'name');
        $contact->email = Arr::get($data, 'email');
        $contact->text = Arr::get($data, 'text');
        $contact->save();

        return back()->withSuccess('Your message has been sent');
    }
}
