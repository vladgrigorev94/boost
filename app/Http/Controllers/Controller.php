<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;

use App\Models\Setting;
use App\Models\Meta;
use App\Models\Block;
use App\Models\Game;
use App\Models\Social;
use View;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    public function __construct() {
        $this->loadSettings();
        $this->loadMeta();
        $this->loadBlocks();
        $this->loadGames();
        $this->loadSocials();
    }

    private function loadSettings() {
        $settings = Setting::find(Setting::MAIN_ID);
        
        if($settings !== null) {
            config([
                'contact_email' => $settings->contact_email,
                'contact_address' => $settings->contact_address,
                'copyright' => $settings->copyright,
            ]);
        }
    }

    private function loadMeta() {
        $uri = \Arr::get($_SERVER, 'REQUEST_URI');

        View::share('meta_title', '');
        View::share('meta_description', '');
        View::share('meta_h1', '');
        View::share('thumb', '');

        if($meta = Meta::where('uri', $uri)->first()) {
            View::share('meta_title', $meta->meta_title);
            View::share('meta_description', $meta->meta_description);
            View::share('meta_h1', $meta->meta_h1);
            View::share('thumb', $meta->thumb);
        }
    }

    private function loadBlocks() {
        $blocks = Block::get();
        $blockValues = [];

        foreach($blocks as $block) {
            $blockValues['block_' . (string)$block->id] = $block;
        }

        config(['blocks' => $blockValues]);
    }

    private function loadGames() {
        $games = Game::get();

        config([
            'games' => $games,
        ]);
    }

    private function loadSocials() {
        $socials = Social::get();

        config([
            'socials' => $socials,
        ]);
    }
}
