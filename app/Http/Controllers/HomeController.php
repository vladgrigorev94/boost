<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Order;
use App\Models\User;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();

        $this->middleware(['auth','verified']);
    }

    public function index()
    {
        $user = \Auth::user();
        $is_user = (!$user->role);
        
        $orders = Order::where('status', \App\Models\Order::STATUS_CREATED)
            ->with('options', 'client', 'prof', 'tarif', 'tarif.game');

        if($is_user) {
            $orders = $orders->where('client_id', $user->id);
        }

        $orders = $orders->get();

        return view('profile.index', compact('user', 'orders'));
    }

    public function inprogress() {
        $user = \Auth::user();
        $is_user = (!$user->role);
        
        $orders = Order::select()
            ->with('options', 'client', 'prof', 'tarif', 'tarif.game');

        if($is_user) {
            $orders = $orders->where('client_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_INPROGRESS);
        } else {
            $orders = $orders->where('prof_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_INPROGRESS);
        }

        $orders = $orders->get();

        return view('profile.exchange.inprogress', compact('user', 'orders'));
    }

    public function endorders() {
        $user = \Auth::user();
        $is_user = (!$user->role);
        
        $orders = Order::select()
            ->with('options', 'client', 'prof', 'tarif', 'tarif.game');

        if($is_user) {
            $orders = $orders
                ->where('client_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_SUCCESS)
                ->orWhere('client_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_FAILURE);
        } else {
            $orders = $orders
                ->where('prof_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_SUCCESS)
                ->orWhere('prof_id', $user->id)
                ->where('status', \App\Models\Order::STATUS_FAILURE);
        }

        $orders = $orders->get();

        return view('profile.exchange.endorders', compact('user', 'orders'));
    }

    public function transactions() {
        $user = \Auth::user();

        return view('profile.balance.transactions', compact('user'));
    }

    public function paymentrequests() {
        $user = \Auth::user();

        return view('profile.balance.payment_request', compact('user'));
    }

    public function edit()
    {
        $user = \Auth::user();
        
        return view('profile.edit', compact('user'));
    }

    public function showOrder(Request $request)
    {
        $user = \Auth::user();
        $id = $request->id;
        $is_user = (!$user->role);

        $order = Order::select()->where('id', $id);
        //->where('status', '>', Order::STATUS_CREATED);

        if($is_user) {
            $order = $order->where('client_id', $user->id);
        } else {
            $order = $order->where('prof_id', $user->id);
        }

        $order = $order->firstOrFail();
        
        return view('profile.order', compact('user', 'order'));
    }

    public function takeOrder(Request $request) {
        $data = $request->all();
        $user = \Auth::user();

        $order = Order::where('status', Order::STATUS_CREATED)
            ->firstOrFail();

        $order->status = Order::STATUS_INPROGRESS;
        $order->prof_id = $user->id;
        $order->save();
        // dd($order);
        return back()->withSuccess('Has been taken.');
    }

    public function endOrder(Request $request) {
        $data = $request->all();
        $user = \Auth::user();

        $order = Order::where('status', Order::STATUS_INPROGRESS)
            ->firstOrFail();

        $order->status = \Arr::get($data, 'status');
        $order->prof_id = $user->id;
        $order->save();

        return back()->withSuccess('Has been taken.');
    }
}
