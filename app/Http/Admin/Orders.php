<?php

namespace App\Http\Admin;

use AdminColumn;
use AdminDisplay;
use AdminForm;
use AdminFormElement;
use SleepingOwl\Admin\Contracts\DisplayInterface;
use SleepingOwl\Admin\Contracts\FormInterface;
use SleepingOwl\Admin\Contracts\Initializable;
use SleepingOwl\Admin\Section;
use SleepingOwl\Admin\Form\FormElements;

class Orders extends Section implements Initializable
{
    /**
     * @var \App\Role
     */
    protected $model;

    /**
     * Initialize class.
     */
    public function initialize()
    {

    }

    /**
     * @return string
     */
    public function getIcon()
    {
        return 'fa fa-group';
    }

    /**
     * @return string|\Symfony\Component\Translation\TranslatorInterface
     */
    public function getTitle()
    {
        return 'List';
    }

    /**
     * @return DisplayInterface
     */
    public function onDisplay()
    {
        return AdminDisplay::table()
            ->setHtmlAttribute('class', 'table-primary')
            ->setColumns(
                [
                    AdminColumn::text('id', 'ID')->setWidth('30px'),
                    AdminColumn::text('client.name', 'Client')->setWidth('30px'),
                    AdminColumn::text('prof.name', 'Prof.')->setWidth('30px'),
                    AdminColumn::text('tarif.name', 'Product')->setWidth('30px'),
                    AdminColumn::text('amount', 'Price')->setWidth('30px'),
                    AdminColumn::text('min_rating', 'From Rating')->setWidth('30px'),
                    AdminColumn::text('max_rating', 'To Rating')->setWidth('30px'),
                    AdminColumn::custom('Status', function($item) {
                        switch($item->status) {
                            case \App\Models\Order::STATUS_CREATED:
                                return 'Created';
                            case \App\Models\Order::STATUS_INPROGRESS:
                                return 'In progress';
                            case \App\Models\Order::STATUS_SUCCESS:
                                return 'Success';
                            case \App\Models\Order::STATUS_FAILURE:
                                return 'Failure';
                        }
                    })->setWidth('30px'),
                ]
            )->setApply(function ($query) {
                $query->orderBy('id', 'DESC');
            })->paginate(20);
    }

    /**
     * @param int $id
     *
     * @return FormInterface
     */
    public function onEdit($id)
    {
        $tabs = AdminDisplay::tabbed();

        $tabs->setTabs(function ($id) {
            $tabs = [];

            $tabs[] = AdminDisplay::tab(new FormElements([
                AdminFormElement::select('tarif_id', 'Product', \App\Models\Tarif::class)
                    ->setDisplay('name')->required(),
                AdminFormElement::select('client_id', 'Client', \App\Models\User::class)
                    ->setDisplay('name')->required(),
                AdminFormElement::select('prof_id', 'Prof', \App\Models\User::class)
                    ->setDisplay('name'),
                AdminFormElement::number('amount', 'Price')->required(),
                AdminFormElement::number('max_rating', 'Max.Rating')->required(),
                AdminFormElement::number('min_rating', 'Max.Rating')->required(),
                // AdminFormElement::multiselect('options', 'Options', \App\Models\OrderOption::class)
                //     ->setDisplay('option.name')->required(),

                AdminFormElement::manyToMany('options', [
                    // AdminFormElement::select('option_id', 'Option', \App\Models\Option::class)
                    //     ->setDisplay('name'),
                ])->setDisplay('name')->setLabel('Options')
            ]))->setLabel('Common');

            return $tabs;
        });

        $form = AdminForm::panel()
            ->addHeader([
                $tabs
            ]);

        return $form;
    }

    /**
     * @return FormInterface
     */
    public function onCreate()
    {
        return $this->onEdit(null);
    }

    public function getCreateTitle()
    {
        return 'Add';
    }

    public function isDeletable(\Illuminate\Database\Eloquent\Model $model)
    {
        return false;
    }

    public function isCreatable()
    {
        return true;
    }

    public function isEditable(\Illuminate\Database\Eloquent\Model $model)
    {
        return true;
    }
}
