<?php

use SleepingOwl\Admin\Navigation\Page;

return [
    [
        'title' => 'Settings',
        'icon'  => 'fas fa-list',
        'url'   => '/admin/settings/1/edit',
        'priority' => 998,
    ],
    [
        'title' => 'Main page',
        'icon'  => 'fas fa-undo',
        'url'   => '/',
        'priority' => 999,
    ],
    [
        'title' => 'Logout',
        'icon'  => 'fas fa-door-open',
        'url'   => '/logout',
        'priority' => 1000,
    ],
    [
        'title' => 'Content',
        'icon'  => 'fas fa-list',
        'pages' => [
            (new Page(\App\Models\Page::class))
                ->setIcon('fa fa-list')
                ->setPriority(98),
            (new Page(\App\Models\Meta::class))
                ->setIcon('fa fa-list')
                ->setPriority(97),
            (new Page(\App\Models\Block::class))
                ->setIcon('fa fa-list')
                ->setPriority(96),
            (new Page(\App\Models\Game::class))
                ->setIcon('fa fa-list')
                ->setPriority(94),
            (new Page(\App\Models\Contact::class))
                ->setIcon('fa fa-list')
                ->setPriority(95),
            (new Page(\App\Models\Blog::class))
                ->setIcon('fa fa-list')
                ->setPriority(93),
            (new Page(\App\Models\Social::class))
                ->setIcon('fa fa-list')
                ->setPriority(99),
        ],
        'priority' => 1,
    ],
    [
        'title' => 'Users',
        'icon'  => 'fas fa-list',
        'pages' => [
            (new Page(\App\Models\User::class))
                ->setIcon('fa fa-list')
                ->setPriority(99),
            (new Page(\App\Models\Review::class))
                ->setIcon('fa fa-list')
                ->setPriority(98),
        ],
        'priority' => 3,
    ],
    [
        'title' => 'Orders',
        'icon'  => 'fas fa-list',
        'pages' => [
            (new Page(\App\Models\Tarif::class))
                ->setIcon('fa fa-list')
                ->setPriority(98),
            (new Page(\App\Models\Order::class))
                ->setIcon('fa fa-list')
                ->setPriority(99),
            (new Page(\App\Models\OptionGroup::class))
                ->setIcon('fa fa-list')
                ->setPriority(100),
            (new Page(\App\Models\Option::class))
                ->setIcon('fa fa-list')
                ->setPriority(101),
        ],
        'priority' => 2,
    ],
];
