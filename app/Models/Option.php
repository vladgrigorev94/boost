<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Option extends Model
{
    use HasFactory;

    public function group() {
        return $this->belongsTo(\App\Models\OptionGroup::class, 'group_id', 'id');
    }

    public function orders()
    {
        return $this->belongsToMany(\App\Models\Order::class, 'orders_options', 'option_id', 'order_id');
    }
}
