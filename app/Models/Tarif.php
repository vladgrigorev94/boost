<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Tarif extends Model
{
    use HasFactory;

    public function game() {
        return $this->belongsTo(\App\Models\Game::class);
    }

    public function groups() {
        return $this->hasMany(\App\Models\OptionGroup::class);
    }
}
