@extends('layouts.index')
@section('content')

<link rel="icon" type="image/png" href="assets/images/favicon/favicon-32x32.png" sizes="32x32">
<link rel="icon" type="image/png" href="assets/images/favicon/favicon-16x16.png" sizes="16x16">

<div class="boxed_wrapper">

    @include('components.header')
    @include('components.pages')
    <gameslide class="gameslide">

        <div class="gameslide-aling">
            <div class="gameslide-block">
                <img src="/assets/images/game-bg/apexlarge.png" class="game-large">
            </div>
        </div>

        <div class="features-title-2">
            <div class="features-title-aling">
                <div class="features-title-slog" style="margin-left: 587px!important;">
                    <img src="/assets/images/howit.gif" class="features-ico">
                    <p>Contacts</p>
                </div>
            </div>
        </div>

        <div class="step-by-step s-b-s-active-2" style="padding-bottom: 150px;">

            <div class="step-by-step-flex">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/icons/job.png" class="step-by-step-ico">
                        <p>Job</p>
                    </div>
                    <p>Email: support@boostingservice.gg
                       <br>Discord: boostingservicecc#2852</p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/icons/location.png" class="step-by-step-ico">
                        <p>Location</p>
                    </div>
                    <p>Suite 2, 5 ST Vincent Street, Scotland UK EH3 6SW
                       <br></p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/icons/mail.png" class="step-by-step-ico">
                        <p>Customer service</p>
                    </div>
                    <p>Email: support@boostingservice.gg 
                       <br>Discord: boostingservicecc#2852</p>
                </div>
            </div>

            <div class="step-by-step-flex">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/minigun.png" class="step-by-step-ico" style="width: 60px!important;">
                        <p>Power</p>
                    </div>
                    <p>Go to check-out and add funds to purchase the 
                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/minigun.png" class="step-by-step-ico" style="width: 60px!important;">
                        <p>Create</p>
                    </div>
                    <p>Go to check-out and add funds to purchase the 
                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>
                </div>
    
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/minigun.png" class="step-by-step-ico" style="width: 60px!important;">
                        <p>Boost</p>
                    </div>
                    <p>Go to check-out and add funds to purchase the 
                       <br> (PayPal, Visa, Mastercard, Wechat, Bitcoin and more)</p>
                </div>
            </div>
        

        </div>

    </gameslide>
    @include('components.footer')
</div>
