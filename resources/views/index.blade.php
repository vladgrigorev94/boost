@extends('layouts.index')

@section('content')

<div class="boxed_wrapper">
    @include('components.header')
    @include('components.pages')
    <div class="home-img-bg">
        <img src="/assets/images/details/bgh.png" class="">
    </div>
    <serviceblock class="service-block">
        <div class="service-block-aling">
            @foreach(config('games') as $game)
            <div>
                <div class="block-s">
                    <p>{{$game->name}}</p>
                    <div>
                        @foreach($game->tarifs as $tarif)
                        <div class="service-1">
                            <a href="{{route('game', ['name' => $game->slug])}}">{{$tarif->name}}</a>
                            <img src="/assets/images/arrow.png" class="service-icon">
                        </div>
                        @endforeach
                    </div>
                    @if($game->animation)
                        <img src="{{url($game->animation)}}" class="game-bg">
                    @endif
                </div>
            </div>
            @endforeach
        </div>
    </serviceblock>
    <features class="features">
        <div>
            <img src="" class="">
            <div class="features-title">
                <div class="features-title-aling">
                    <div class="features-title-slog">
                        <img src="/assets/images/features.png" class="features-ico">
                        <p>Our Features</p>
                    </div>
                </div>
            </div>
            <div class="features-blocks">
                <div class="features-pack-block">
                    <img src="/assets/images/block-ico.png" class="features-blocks-ico">
                    <div class="features-block">
                        <img src="/assets/images/pulse-g.png" class="features-ico">
                        <h1>{{\Arr::get(config('blocks'), 'block_1.title')}}</h1>
                        <p>
                            {{\Arr::get(config('blocks'), 'block_1.content')}}
                        </p>
                    </div>
                </div>
                <div class="features-pack-block">
                    <div class="features-block">
                        <img src="/assets/images/pulse-g.png" class="features-ico">
                        <h1>{{\Arr::get(config('blocks'), 'block_2.title')}}</h1>
                        <p>
                            {{\Arr::get(config('blocks'), 'block_2.content')}}
                        </p>
                    </div>
                    <img src="/assets/images/block-ico-or.png" class="features-blocks-ico-2">
                </div>
            </div>
            <div class="features-blocks">
                <div class="features-pack-block">
                    <img src="/assets/images/block-ico-p1.png" class="features-blocks-ico">
                    <div class="features-block">
                        <img src="/assets/images/pulse-g.png" class="features-ico">
                        <h1>{{\Arr::get(config('blocks'), 'block_3.title')}}</h1>
                        <p>
                            {{\Arr::get(config('blocks'), 'block_3.content')}}
                        </p>
                    </div>
                </div>
                <div class="features-pack-block">
                    <div class="features-block">
                        <img src="/assets/images/pulse-g.png" class="features-ico">
                        <h1>{{\Arr::get(config('blocks'), 'block_4.title')}}</h1>
                        <p>
                            {{\Arr::get(config('blocks'), 'block_4.content')}}
                        </p>
                    </div>
                    <img src="/assets/images/block-ico-p2.png" class="features-blocks-ico-2">
                </div>
            </div>
        </div>
    </features>
    <commblock class="comm-blocks">
        <div class="comm-blocks-aling">
            <div class="comm-blocks-center">
                <img src="/assets/images/logocom.gif" class="logo-com">
                @foreach($reviews as $chunk)
                <div class="comm" style="margin-right: 620px;">
                    <h2 style="text-align: left!important;">Review from users</h2>
                    @foreach($chunk as $review)
                    <div class="comm-block">
                        <img src="{{isset($review->avatar) ? $review->avatar : '/assets/images/coom-ico.png'}}" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>{{isset($review->user->name) ? $review->user->name : $review->name}}</h3>
                                <p>Verified</p>
                            </div>
                            <p>{{$review->text}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
            </div>
            <div class="">
                <a href="{{route('reviews')}}" class="more-comm">Load more</a>
            </div>
        </div>
    </commblock>
    <boosting class="boosting-info">
        <div class="features-title">
            <div class="features-title-aling">
                <div class="features-title-slog">
                    <img src="/assets/images/features.png" class="features-ico">
                    <p>Boosting Platform</p>
                </div>
            </div>
        </div>
        <div class="boosting-blocks">
            <div class="boosting-pack-block">
                <img src="/assets/images/block-ico-red.png" class="boosting-blocks-ico">
                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>{{\Arr::get(config('blocks'), 'block_5.title')}}</h1>
                    <p>
                        {{\Arr::get(config('blocks'), 'block_5.content')}}
                    </p>
                </div>
            </div>
            <div class="boosting-pack-block">
                <img src="/assets/images/block-ico-red.png" class="boosting-blocks-ico">

                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>{{\Arr::get(config('blocks'), 'block_6.title')}}</h1>
                    <p>
                        {{\Arr::get(config('blocks'), 'block_6.content')}}
                    </p>
                </div>
            </div>
            <div class="boosting-pack-block">
                <img src="/assets/images/block-ico-red.png" class="boosting-blocks-ico">
                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>{{\Arr::get(config('blocks'), 'block_7.title')}}</h1>
                    <p>
                        {{\Arr::get(config('blocks'), 'block_7.content')}}
                    </p>
                </div>
            </div>
        </div>
        <div class="boosting-blocks" style="justify-content: center;">
            <div class="boosting-pack-block" style="margin-right: 150px;">
                <img src="/assets/images/block-ico-p1.png" class="boosting-blocks-ico">
                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>{{\Arr::get(config('blocks'), 'block_8.title')}}</h1>
                    <p>
                        {{\Arr::get(config('blocks'), 'block_8.content')}}
                    </p>
                </div>
            </div>
            <div class="boosting-pack-block">
                <img src="/assets/images/block-ico-p1.png" class="boosting-blocks-ico">

                <div class="boosting-block">
                    <img src="/assets/images/minigun.png" class="boosting-ico">
                    <h1>{{\Arr::get(config('blocks'), 'block_9.title')}}</h1>
                    <p>
                        {{\Arr::get(config('blocks'), 'block_9.content')}}
                    </p>
                </div>
            </div>
        </div>
    </boosting>
    <moreinfo-2>
        <div class="features-title-2">
            <div class="features-title-aling">
                <div class="features-title-slog" style="margin-left: 587px!important;">
                    <img src="/assets/images/bullet.gif" class="features-ico" style="height: 30px!important;">
                    <p>More information</p>
                </div>
            </div>
        </div>
        <div class="step-by-step s-b-s-active-2" style="padding-bottom: 150px;">
            <div class="step-by-step-flex">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        @if(\Arr::get(config('blocks'), 'block_10.thumb'))
                            <img src="{{\Arr::get(config('blocks'), 'block_10.thumb')}}" class="step-by-step-ico">
                        @endif
                        <p>{{\Arr::get(config('blocks'), 'block_10.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_10.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        @if(\Arr::get(config('blocks'), 'block_11.thumb'))
                            <img src="{{\Arr::get(config('blocks'), 'block_11.thumb')}}" class="step-by-step-ico">
                        @endif
                        <p>{{\Arr::get(config('blocks'), 'block_11.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_11.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        @if(\Arr::get(config('blocks'), 'block_12.thumb'))
                            <img src="{{\Arr::get(config('blocks'), 'block_12.thumb')}}" class="step-by-step-ico">
                        @endif
                        <p>{{\Arr::get(config('blocks'), 'block_12.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_12.content')}}</p>
                </div>
            </div>
            <div class="step-by-step-flex">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        @if(\Arr::get(config('blocks'), 'block_13.thumb'))
                            <img src="{{\Arr::get(config('blocks'), 'block_13.thumb')}}" class="step-by-step-ico">
                        @endif
                        <p>{{\Arr::get(config('blocks'), 'block_13.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_13.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        @if(\Arr::get(config('blocks'), 'block_14.thumb'))
                            <img src="{{\Arr::get(config('blocks'), 'block_14.thumb')}}" class="step-by-step-ico">
                        @endif
                        <p>{{\Arr::get(config('blocks'), 'block_14.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_14.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        @if(\Arr::get(config('blocks'), 'block_15.thumb'))
                            <img src="{{\Arr::get(config('blocks'), 'block_15.thumb')}}" class="step-by-step-ico">
                        @endif
                        <p>{{\Arr::get(config('blocks'), 'block_15.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_15.content')}}</p>
                </div>
            </div>
            <div class="step-by-step-flex">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        @if(\Arr::get(config('blocks'), 'block_16.thumb'))
                            <img src="{{\Arr::get(config('blocks'), 'block_16.thumb')}}" class="step-by-step-ico">
                        @endif
                        <p>{{\Arr::get(config('blocks'), 'block_16.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_16.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        @if(\Arr::get(config('blocks'), 'block_17.thumb'))
                            <img src="{{\Arr::get(config('blocks'), 'block_17.thumb')}}" class="step-by-step-ico">
                        @endif
                        <p>{{\Arr::get(config('blocks'), 'block_17.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_17.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        @if(\Arr::get(config('blocks'), 'block_18.thumb'))
                            <img src="{{\Arr::get(config('blocks'), 'block_18.thumb')}}" class="step-by-step-ico">
                        @endif
                        <p>{{\Arr::get(config('blocks'), 'block_18.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_18.content')}}</p>
                </div>
            </div>
        </div>
    </moreinfo-2>
    @include('components.footer')
</div>
</body>
