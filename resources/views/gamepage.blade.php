@extends('layouts.index')

@section('content')

<div class="boxed_wrapper">
    @include('components.header')
    @include('components.pages')
    <gameslide class="gameslide">
        @if($game->image)
        <div class="gameslide-aling">
            <div class="gameslide-block">
                <img src="{{$game->image}}" class="game-large">
            </div>
        </div>
        @endif
        <div class="features-title-2">
            <div class="features-title-aling">
                <div class="features-title-slog" style="margin-left: 587px!important;">
                    <img src="/assets/images/features.png" class="features-ico">
                    <p>{{$game->name}}</p>
                </div>
            </div>
        </div>
        @foreach($tarifs as $tarifChunk)
        <div class="game-service-block-aling">
            <img src="" class="">
            @foreach($tarifChunk as $tarif)
            <div class="game-service-block">
                <img src="/assets/images/arrow.png" class="game-service-img">
                <div class="game-service-txt-1">
                    <img src="">
                    <p>Badges</p>
                    <h1>{{$tarif->name}}</h1>
                </div>
                <div class="game-service-txt-2">
                    <p>{{$tarif->description}}</p>
                </div>
                <div class="">
                    <div class="create-order-pads-2">
                        <button onclick="window.location.href='{{route('product', ['id' => $tarif->id])}}';" type="submit" id="buy-ord-btn-2">Buy Service</button>
                        <div class="how-much-2"><p>{{$tarif->price}}$</p></div>
                    </div>
                </div>
            </div>
            @endforeach 
        </div>
        @endforeach
        @if(count($reviews))
        <div class="review-details-al">
            <div class="review-details">
                <h2 style="text-align: left!important;">Review from users</h2>
                @foreach($reviews as $chunk)
                <div class="review-details-aling">
                    @foreach($chunk as $review)
                    <div class="comm-block-2">
                        <img src="{{isset($review->avatar) ? '/'.$review->avatar : '/assets/images/coom-ico.png'}}" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>{{isset($review->user->name) ? $review->user->name : $review->name}}</h3>
                                <p>Verified</p>
                            </div>
                            <p>{{$review->text}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
                <a href="{{route('reviews')}}" class="more-comm2">Load more</a>
            </div>
        </div>
        @endif
    </gameslide>
    @include('components.footer')
</div>
