@extends('layouts.index')

@section('content')
<div class="boxed_wrapper">
    @include('components.header')
    @include('components.pages')
    <gameslide class="gameslide">
        @if($game->image)
        <div class="gameslide-aling">
            <div class="gameslide-block">
                <img src="{{url($game->image)}}" class="game-large">
            </div>
        </div>
        @endif
        <div class="features-title-2">
            <div class="features-title-aling">
                <div class="features-title-slog">
                    <img src="/assets/images/features.png" class="features-ico">
                    <p>Create Order</p>
                </div>
            </div>
        </div>
        <div class="create-order-bg">
            <div class="create-order-full">
                <div class="create-order-title">
                    <img src="/assets/images/minigun.png" class="">
                    <p>{{$game->name}}</p>
                    <h1>{{$tarif->name}}</h1>
                </div>
                <form action="{{route('order')}}" method="POST">
                    @csrf
                    <div>
                        <div class="calt-aling">
                            <div class="filter__cost table">
                            <div class="table-title">
                                <div class="table-text-1"><p>Start Rrank</p></div>
                                <img src="/assets/images/shield.png" class="shield-ico">
                                <div class="table-text-2"><p>End Rrank</p></div>
                            </div>
                                <div class="table-aling">
                                    <div class="table-cell">
                                        <input id="priceMin" name="min_rating" type="text" value="0" class="form-control">
                                    </div>
                                    <div class="table-cell">
                                        <input id="priceMax" name="max_rating" type="text" value="{{$tarif->max_rating}}" class="form-control">
                                    </div>
                                </div>
                            </div>
                            <div class="filter__block filter__block--slider">
                                <div class="filter__slider">
                                    <div id="filter__range" class="ui-slider ui-corner-all ui-slider-horizontal ui-widget ui-widget-content">
                                        <span class="ui-slider-handle ui-corner-all ui-state-default" style="left: 35%!important;"></span>
                                        <span  class="ui-slider-handle ui-corner-all ui-state-default" style="margin-left: 0px;"></span>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="order-check-box">
                        @foreach($tarif->groups as $group)
                        <div class="order-check-box-aling">
                            <p>{{$group->name}}</p>
                            <div class="checkbox-aling">
                                @foreach($group->options as $option)
                                <label class="container">{{$option->name}}
                                    <input type="checkbox" checked="checked" name="OPTIONS[{{$option->id}}]" value="1">
                                    <span class="checkmark"></span>
                                </label>
                                @endforeach
                            </div>
                        </div>
                        @endforeach
                    </div>
                    <div class="create-order-pads">
                        <input type="hidden" value="{{$tarif->id}}" name="tarif_id">
                        <input type="hidden" value="{{$tarif->price}}" name="amount">

                        <button type="submit" id="buy-ord-btn">Buy Service</button>
                        <div class="how-much"><p>{{$tarif->price}}$</p></div>
                        <div class="how-much-2"><p>Bonuce: <span style="color: #ffffff;">0 coins</span></p></div>
                    </div>
                </form>
            </div>
        </div>
        @if(count($reviews))
        <div class="review-details">
            <div class="review-details">
                <h2 style="text-align: left!important;">Review from users</h2>
                @foreach($reviews as $chunk)
                <div class="review-details-aling">
                    @foreach($chunk as $review)
                    <div class="comm-block-2">
                        <img src="{{isset($review->avatar) ? '/'.$review->avatar : '/assets/images/coom-ico.png'}}" class="comm-ico">
                        <div class="comm-text-block">
                            <div class="comm-name">
                                <h3>{{isset($review->user->name) ? $review->user->name : $review->name}}</h3>
                                <p>Verified</p>
                            </div>
                            <p>{{$review->text}}</p>
                        </div>
                    </div>
                    @endforeach
                </div>
                @endforeach
                <a href="{{route('reviews')}}" class="more-comm2">Load more</a>
            </div>
        </div>
        @endif
        <div class="step-by-step">
            <div class="step-by-step-flex" >
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_20.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_20.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_21.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_21.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_22.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_22.content')}}</p>
                </div>
            </div>
            <div class="step-by-step-flex"  style="padding-bottom: 150px;">
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_23.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_23.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_24.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_24.content')}}</p>
                </div>
                <div class="step-by-step-blocks">
                    <div class="step-by-step-block">
                        <img src="/assets/images/quest.png" class="step-by-step-ico">
                        <p>{{\Arr::get(config('blocks'), 'block_25.title')}}</p>
                    </div>
                    <p>{{\Arr::get(config('blocks'), 'block_25.content')}}</p>
                </div>
            </div>
        </div>
    </gameslide>
    @include('components.footer')
</div>
