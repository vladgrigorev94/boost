@extends('layouts.profile')

@section('content')
    @if($user->role === \App\Models\User::ROLE_USER)
        @include('profile.roles.user')
    @else
        @include('profile.roles.prof')
    @endif
@endsection
