@extends('layouts.profile')

@section('content')
    <orderlist class="order-list">
        <div class="order-list-menu">
            @include('profile.particles.topmenu')
            <div class="order-title">
                <!-- <h1>all orders</h1> -->
            </div>
        </div>
        <div class="order-list-blocks-3">
            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>You Received:<span style="margin-left: 3px; color: rgb(255, 86, 86);">224$</span></p>
            </div>
            <div class="order-list-block">
                <p>P.Date:<span style="margin-left: 3px; color: white;">11.11.21</span></p>
            </div>
            <div class="order-list-block">
                <div class="screen-shot"></div>
                <p>Screenshot</p>
            </div>
            <div class="order-list-block">
                <p>Pay:<span style="margin-left: 3px; color: white;">4441 4444 4442 3333</span></p>
            </div>
            <div class="order-list-block">
                <p>Comment:<span style="margin-left: 3px; color: white;">Thank for work!</span></p>
            </div>
        </div>
        <div class="order-list-blocks-3">
            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>You Received:<span style="margin-left: 3px; color: rgb(255, 86, 86);">224$</span></p>
            </div>
            <div class="order-list-block">
                <p>P.Date:<span style="margin-left: 3px; color: white;">11.11.21</span></p>
            </div>
            <div class="order-list-block">
                <div class="screen-shot"></div>
                <p>Screenshot</p>
            </div>
            <div class="order-list-block">
                <p>Pay:<span style="margin-left: 3px; color: white;">4441 4444 4442 3333</span></p>
            </div>
            <div class="order-list-block">
                <p>Comment:<span style="margin-left: 3px; color: white;">Thank for work!</span></p>
            </div>
        </div>
        <div class="order-list-blocks-4">
            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>You earned:<span style="margin-left: 3px; color: rgb(92, 255, 86);">1,224$</span></p>
            </div>
            <img src="/assets/images/details/load.gif" style="pointer-events: none;">
            <div class="order-list-block">
                <p>Please:<span style="margin-left: 3px; color: white;">always do job well</span></p>
            </div>
        </div>
        <div class="order-list-blocks-3">
            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>You Received:<span style="margin-left: 3px; color: rgb(255, 86, 86);">224$</span></p>
            </div>
            <div class="order-list-block">
                <p>P.Date:<span style="margin-left: 3px; color: white;">11.11.21</span></p>
            </div>
            <div class="order-list-block">
                <div class="screen-shot"></div>
                <p>Screenshot</p>
            </div>
            <div class="order-list-block">
                <p>Pay:<span style="margin-left: 3px; color: white;">4441 4444 4442 3333</span></p>
            </div>
            <div class="order-list-block">
                <p>Comment:<span style="margin-left: 3px; color: white;">Thank for work!</span></p>
            </div>
        </div>
        <div class="order-list-blocks-4">
            <div class="order-list-block">
                <p>Id:<span style="margin-left: 3px; color: white;">1</span></p>
            </div>
            <div class="order-list-block">
                <p>You earned:<span style="margin-left: 3px; color: rgb(92, 255, 86);">1,224$</span></p>
            </div>
            <img src="/assets/images/details/load.gif" style="pointer-events: none;">
            <div class="order-list-block">
                <p>Please:<span style="margin-left: 3px; color: white;">always do job well</span></p>
            </div>
        </div>
    </orderlist>
    <loyalty  class="loyalprogram">
        <div class="loyalprogram-title">
            <img src="/assets/images/loyal.png" class="loyalprogram-title-ico">
            <h1>Loyalty Program</h1>
        </div>
        <div class="loyalprogram-blocks-aling">
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 1</p>
                    <img src="/assets/images/loyalrang.gif" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2 class="active-h2">You Here</h2>
                </div>
            </div>
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 2</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 3</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 4</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>
            <div class="loyalprogram-blocks">
                <div class="loyalprogram-block">
                    <p>Level 5</p>
                    <img src="/assets/images/loyalrang.png" class="loyalprogram-block-pic">
                    <h2>Life-Time 3% <br> Cashback In BC</h2>
                </div>
                <div class="loyalprogram-block-text">
                    <p>Spent 50 EUR <br> or 5 orders</p>
                    <h2>100 Eur to bonuce</h2>
                </div>
            </div>
        </div>
    </loyalty>
@endsection
