
<div class="boxed_wrapper">
    @include('profile.particles.header')
    <orderlist class="order-list">
        <div class="order-list-menu">
            @include('profile.particles.topmenu')
            <div class="order-title">
                <!-- <h1>all orders</h1> -->
            </div>
        </div>
        @foreach($orders as $key => $order)
            @include('profile.roles.components.task')
        @endforeach
        <div class="order-pads">
            <div class="order-pad-home">
                <a href="">Buy more Services</a>
                <img src="/assets/images/features.png" class="order-pad-home-img">
            </div>
            <div class="order-pad-home">
                <a href="{{route('main')}}">Back to Home</a>
                <img src="/assets/images/features.png" class="order-pad-home-img">
            </div>
        </div>
    </orderlist>
    @include('components.footer')
</div> 
