<profile class="profile">
    <div class="profile-aling">
        <a href="{{route('main')}}">
            <div class="profile-block-1">
                    <img src="/assets/images/details/logo.gif" class="footer-logo">
            </div>
        </a>
        <div class="profile-menu">
            <div class="profile-avatar-aling">
                <div class="profile-avatar-2">
                    <img class="profile-ava-3" src="/assets/images/avatars/1.png">
                </div>
            </div>
            <div class="profile-menu-block">
                <h2>{{\Auth::user()->name}} | @include('components.role')</h2>
                <a href="mailto:{{\Auth::user()->email}}">{{\Auth::user()->email}}</a>
            </div>
        </div>
        @include('profile.particles.total')
    </div>
</profile>
