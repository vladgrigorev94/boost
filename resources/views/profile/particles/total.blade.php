@if(\Auth::user()->role === \App\Models\User::ROLE_USER)
    <div class="profile-menu">
        <div class="profile-avatar-aling">
            <div class="profile-avatar">
                <img class="profile-ava" src="/assets/images/balance.png">
            </div>
        </div>
        <div class="balance-menu-block">
            <h2>Balance:</h2>
            <div class="blance-available-aling">
                <div class="balance-available-4">
                    <p>Bonuce: <span style="color: white;">424 coins</span></p>
                </div>
                <div class="balance-available-2">
                    <p>Spend: <span style="color: white;">1,224$</span></p>
                </div>
            </div>
        </div>
        <div class="out-edit">
            <div class="out">
                <a href="{{route('logout')}}">Log out</a>
                <img src="/assets/images/log.png" class="out-icon">
            </div>
            <div class="edit-prof">
                <a href="{{route('home.edit')}}">Edit Profile</a>
                <img src="/assets/images/edit.png" class="edit-prof-icon">
            </div>
        </div>
    </div>
@else
    <div class="profile-menu">
        <div class="profile-avatar-aling">
            <div class="profile-avatar">
                <img class="profile-ava" src="/assets/images/balance.png">
            </div>
        </div>
        <div class="balance-menu-block">
            <h2>Balance:</h2>
            <div class="blance-available-aling">
                <div class="balance-available">
                    <p>Available: <span style="color: white;">424$</span></p>
                </div>
                <div class="balance-available-2">
                    <p>Total: <span style="color: white;">1,224$</span></p>
                </div>
                <div class="balance-available-3">
                    <p>Earned: <span style="color: white;">1,224$</span></p>
                </div>
            </div>
        </div>
        <div class="out-edit">
            <div class="out">
                <a href="{{route('logout')}}">Log out</a>
                <img src="/assets/images/log.png" class="out-icon">
            </div>
            <div class="edit-prof">
                <a href="{{route('home.edit')}}">Edit Profile</a>
                <img src="/assets/images/edit.png" class="edit-prof-icon">
            </div>
        </div>
    </div>
@endif
