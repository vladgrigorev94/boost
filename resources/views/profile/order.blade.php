@extends('layouts.index')

@section('content')
<div class="boxed_wrapper">
    @include('components.header')
    @include('components.pages')
    
    <?php $key = 0; ?>
    @include('profile.roles.components.chat')
    @include('components.footer')
</div>
