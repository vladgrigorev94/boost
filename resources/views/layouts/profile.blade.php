<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>{{isset($meta_title) ? $meta_title . ' | ' : ''}}Boosting Service</title>

        <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="description" content="{{isset($meta_description) ? $meta_description : ''}}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
        
        <meta property="og:url" content="{{url('/')}}">
        <meta property="og:type" content="website">
        <meta property="og:title" content="{{isset($meta_title) ? $meta_title . ' | ' : ''}}Boosting Service">
        <meta property="og:description" content="{{isset($meta_description) ? $meta_description : ''}}">
        @if(isset($thumb))
            <meta property="og:image" content="{{url($thumb)}}">
        @endif

        <!-- responsive meta -->
        <!-- <meta name="viewport" content="width=device-width, initial-scale=1"> -->
        <!-- For IE -->
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        

        <link rel="stylesheet" href="/assets/css/style.css">
        <!-- <link rel="stylesheet" href="/assets/css/font-awesome.min.css"> -->
        <!-- <link rel="stylesheet" href="/assets/css/responsive.css"> -->

        <!-- Favicon -->
        <link rel="apple-touch-icon" sizes="57x57" href="/assets/images/favicon/apple-icon-57x57.png">
        <link rel="apple-touch-icon" sizes="60x60" href="/assets/images/favicon/apple-icon-60x60.png">
        <link rel="apple-touch-icon" sizes="72x72" href="/assets/images/favicon/apple-icon-72x72.png">
        <link rel="apple-touch-icon" sizes="76x76" href="/assets/images/favicon/apple-icon-76x76.png">
        <link rel="apple-touch-icon" sizes="114x114" href="/assets/images/favicon/apple-icon-114x114.png">
        <link rel="apple-touch-icon" sizes="120x120" href="/assets/images/favicon/apple-icon-120x120.png">
        <link rel="apple-touch-icon" sizes="144x144" href="/assets/images/favicon/apple-icon-144x144.png">
        <link rel="apple-touch-icon" sizes="152x152" href="/assets/images/favicon/apple-icon-152x152.png">
        <link rel="apple-touch-icon" sizes="180x180" href="/assets/images/favicon/apple-icon-180x180.png">
        <link rel="icon" type="image/png" sizes="192x192"  href="/assets/images/favicon/android-icon-192x192.png">
        <link rel="icon" type="image/png" sizes="32x32" href="/assets/images/favicon/favicon-32x32.png">
        <link rel="icon" type="image/png" sizes="96x96" href="/assets/images/favicon/favicon-96x96.png">
        <link rel="icon" type="image/png" sizes="16x16" href="/assets/images/favicon/favicon-16x16.png">
        <link rel="manifest" href="/assets/images/favicon/manifest.json">
        <meta name="msapplication-TileColor" content="#ffffff">
        <meta name="msapplication-TileImage" content="/assets/images/favicon/ms-icon-144x144.png">
        <meta name="theme-color" content="#ffffff">

        <!-- Fixing Internet Explorer-->
        <!--[if lt IE 9]>
            <script src="http://html5shiv.googlecode.com/svn/trunk/html5.js"></script>
            <script src="assets/js/html5shiv.js"></script>
        <![endif]-->

        <script src="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.js" integrity="sha512-lOrm9FgT1LKOJRUXF3tp6QaMorJftUjowOWiDcG5GFZ/q7ukof19V0HKx/GWzXCdt9zYju3/KhBNdCLzK8b90Q==" crossorigin="anonymous"></script>
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/noty/3.1.4/noty.min.css" integrity="sha512-0p3K0H3S6Q4bEWZ/WmC94Tgit2ular2/n0ESdfEX8l172YyQj8re1Wu9s/HT9T/T2osUw5Gx/6pAZNk3UKbESw==" crossorigin="anonymous" />
            
        @if (session('errors') && session('errors')->getMessages())
            @foreach(session('errors')->getMessages() as $error)
                @if(\Arr::get($error, 0) !== null)
                <script>
                    // $(function() {
                        new Noty({
                            type: 'error',
                            text: '{!! \Arr::get($error, 0) !!}',
                            timeout: 3000,
                        }).show();
                    // });
                </script>
                @endif
            @endforeach

            <?php //\Session::flash('errors'); ?>
        @endif

        @if (session('successes') && session('successes')->getMessages())
            @foreach(session('successes')->getMessages() as $success)
                @if(\Arr::get($success, 0) !== null)
                <script>
                    // $(function() {
                        new Noty({
                            type: 'success',
                            text: "{!! \Arr::get($success, 0) !!}",
                            timeout: 3000,
                        }).show();
                    // });
                </script>
                @endif
            @endforeach

            <?php //\Session::flash('successes'); ?>
        @endif

        @if(session('success'))
            @if(is_array(session('success')))
                @foreach (session('success') as $message)
                    <script>
                        // $(function() {
                            new Noty({
                                type: 'success',
                                text: "{{ $message }}",
                                timeout: 3000,
                            }).show();
                        // });
                    </script>
                @endforeach
            @else
                <script>
                    // $(function() {
                        new Noty({
                            type: 'success',
                            text: "{{ session('success') }}",
                            timeout: 3000,
                        }).show();
                    // });
                </script>
            @endif
        @endif

        @if(session('status'))
            @if(is_array(session('status')))
                @foreach (session('status') as $message)
                    <script>
                        // $(function() {
                            new Noty({
                                type: 'success',
                                text: "{{ $message }}",
                                timeout: 3000,
                            }).show();
                        // });
                    </script>
                @endforeach
            @else
                <script>
                    // $(function() {
                        new Noty({
                            type: 'success',
                            text: "{{ session('status') }}",
                            timeout: 3000,
                        }).show();
                    // });
                </script>
            @endif
        @endif
    </head>
    <body>
        @yield('content')
    </body>
</html>
